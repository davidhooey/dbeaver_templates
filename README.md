# dbeaver templates

A set of Oracle, SQL Server and PostgreSQL database templates.

## Import

1. Launch DBeaver.

2. Go to Preferences -> Editors -> SQL Editor -> Templates.

3. Select Import... and select the dbeaver_templates.xml.
